# inicio para actualizar librerias o vendor
composer install

# correr migracion
php artisan migrate

# comandos iniciales git 
git clone git@gitlab.com:fabiorojas90/blog.git
git pull -> para obtener cambios realizados en git
# comando para subir cambios realizados
git add . -> todos los cambios realizados en el proyectp
git commit -m "descripcion" -> mensaje en la rama del git
git push origin master(rama) -> subir cambios a git

# correr laravel
php artisan serve
