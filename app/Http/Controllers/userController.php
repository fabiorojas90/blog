<?php

namespace App\Http\Controllers;

use App\User;
use Exception;
use Illuminate\Http\Request;

class userController extends Controller
{
    public function getAll(){
        try{
            $user = User::getAll();
            return response()->json(array('data'=>$user,'status'=>false));
        }catch(Exception $ex){
            return response()->json(array('msg'=>"exception ".$ex->getMessage(),'status'=>true));
        }
    }

    public function postUser(Request $request){
        try{
            $user = User::postUser($request->all());
            return response()->json(array('user'=>$user,'status'=>false));
        }catch(Exception $ex){
            return response()->json(array('msg'=>"exception ".$ex->getMessage(),'status'=>true));
        }
    }

    public function putUser(Request $request,$id){
        try{
            $user = User::putUser($request->all(),$id);
            return response()->json(array('user'=>$user,'status'=>false));
        }catch(Exception $ex){
            return response()->json(array('msg'=>"exception ".$ex->getMessage(),'status'=>true));
        }
    }

}
